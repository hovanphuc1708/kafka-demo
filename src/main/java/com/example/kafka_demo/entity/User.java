package com.example.kafka_demo.entity;

import lombok.Data;

@Data
public class User {
    private String name;
    private String email;
}
