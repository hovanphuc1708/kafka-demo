package com.example.kafka_demo.controller;

import com.example.kafka_demo.service.ProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/demo")
public class DemoKafkaController {
    @Autowired
    private ProducerService producerService;


    @PostMapping("/publish")
    public void sendMessageToKafkaTopic(@RequestParam(value = "message")String message){
        producerService.send(message);
    }



}
